package com.akalipetis.android_workshop_two;


import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotosetsListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private FlickrPhotosetAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*
         * Δημιουργούμε το layout μας, χρησιμοποιώντας τον LayoutInflater που μας δίνεται και το resource έχουμε
         * φτιάξει.
         */
        return inflater.inflate(R.layout.fragment_photosets_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        /*
         * Η μέθοδος αυτή καλείται όταν έχει δημιουργηθεί το view μας, το οποίο μας το δίνει και σαν παράμετρο.
         * Βρίσκουμε τη λίστα μέσα στο view και μετά της δίνουμε τον adapter.
         */
        ListView listView = (ListView) view.findViewById(R.id.fragment_photosets_list);
        mAdapter = new FlickrPhotosetAdapter(getActivity());
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);

        /*
         * Ξεκινάμε το task για να πάρουμε τα photosets από το server.
         */
        FlickrPhotosetTask task = new FlickrPhotosetTask();
        task.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View v, int position,
                            long id) {
        /*
         * Η μέθοδος αυτή καλείται όταν πατηθεί ένα κελί της λίστας. Παίρνουμε μία αναφορά στο Activity το οποίο
         * βρισκόμαστε και καλούμε την αντίστοιχη μέθοδο για να το ενημερώσουμε ότι επιλέχθηκε το συγκεκριμένο ID.
         */
        MainActivity activity = (MainActivity) getActivity();
        activity.setPhotosetId(id);
    }

    private class FlickrPhotosetTask extends
            AsyncTask<Void, Void, ArrayList<FlickrPhotoset>> {

        @Override
        protected ArrayList<FlickrPhotoset> doInBackground(Void... params) {
            /*
             * Χρησιμοποιούμε το NetworkHelper για να πάρουμε τα photosets από το server.
             */
            return NetworkHelper.getPhotosets();
        }

        @Override
        protected void onPostExecute(ArrayList<FlickrPhotoset> result) {
            /*
             * Δρούμε ανάλογα με το τι μας επέστρεψε η getPhotosets - το οποίο δίνεται σαν παράμετρος result
             * σε αυτή τη συνάρτηση. Αν δεν είναι null περνάμε τα δεδομένα στον adapter,
             * αλλιώς - δηλαδή αν κάτι πήγε λάθος - τυπώνουμε αντίστοιχο μήνυμα.
             */
            if (result != null) {
                mAdapter.setPhotosets(result);
            } else {
                Toast.makeText(getActivity(), "Could not fetch Photosets from Network.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
