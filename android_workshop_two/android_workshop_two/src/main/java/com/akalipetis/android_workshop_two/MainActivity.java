package com.akalipetis.android_workshop_two;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
         * Απλά δημιουργούμε το layout το οποίο περιέχει τα απαραίτητα Fragments. Πλέον όλη η δουλειά γίνεται εκεί.
         */
        setContentView(R.layout.activity_main);
    }

    /**
     * Method used to update the selected photoset ID. Finds the fragment containing the grid of photos and updates
     * it.
     *
     * @param id The ID of the selected photoset.
     */
    public void setPhotosetId(long id) {
        /*
         * Χρησιμοποιούμε τον FragmentManager ο οποίος διαχειρίζεται όλα τα Fragments αυτού του Activity και
         * βρίσκουμε το Fragment που μας ενδιαφέρει. Μετά το ανανεώνουμε. Αν το Fragment αυτό είναι null,
         * αν δηλαδή δεν υπάρχει μέσα στο layout (δείτε layout/activity_main.xml και layout-land/activity_main.xml)
         * τότε ξεκινάμε το PhotosGridActivity.
         */
        PhotosGridFragment f = (PhotosGridFragment) getFragmentManager().findFragmentById(R.id.activity_main_fragment_photos_grid);
        if (f != null) {
            f.setPhotosetId(id);
        } else {
            Intent i = new Intent(this, PhotosGridActivity.class);
            i.putExtra("photoset_id", id);
            startActivity(i);
        }

    }
}
