package com.akalipetis.android_workshop_two;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;

public class FlickrPhotosAdapter extends BaseAdapter {

    private ArrayList<FlickrPhoto> mPhotos;
    private final Context mContext;

    public FlickrPhotosAdapter(Context context) {
        mContext = context;
    }

    /**
     * Method used to dynamically update the adapter. If null is given, the data of the list gets invalidated,
     * otherwise the list gets notified of the update.
     *
     * @param photos An ArrayList containing the photos to use.
     */
    public void setPhotos(ArrayList<FlickrPhoto> photos) {
        mPhotos = photos;
        if (mPhotos == null) {
            notifyDataSetInvalidated();
        } else {
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return (mPhotos == null) ? 0 : mPhotos.size();
    }

    @Override
    public Object getItem(int position) {
        return mPhotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(mPhotos.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(R.drawable.ic_launcher);
        ImageLoadingTask task = new ImageLoadingTask(imageView);
        task.execute(mPhotos.get(position).getSmallPhotoUrl());
        return imageView;
    }

    private class ImageLoadingTask extends
            AsyncTask<String, Void, BitmapDrawable> {

        private final ImageView mImage;

        public ImageLoadingTask(ImageView image) {
            mImage = image;
        }

        @Override
        protected BitmapDrawable doInBackground(String... params) {
            String url = params[0];
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(url);
            HttpResponse response;
            try {
                response = client.execute(get);
                Resources res = mContext.getResources();
                return new BitmapDrawable(res, response.getEntity()
                        .getContent());
            } catch (ClientProtocolException e) {
                return null;
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(BitmapDrawable result) {
            super.onPostExecute(result);
            if ((result != null) && (mImage != null))
                mImage.setImageDrawable(result);
        }
    }
}
