package com.akalipetis.android_workshop_two;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Class used to fetch data from the Flickr server, using the Internet.
 */
public class NetworkHelper {

    private static final String BASE_FLICKR_URL = "http://api.flickr.com/services/rest/";

    private static final String KEY_FORMAT = "format";
    private static final String KEY_METHOD = "method";
    private static final String KEY_NO_JSON_CALLBACK = "nojsoncallback";
    private static final String KEY_API_KEY = "api_key";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_PHOTOSET_ID = "photoset_id";

    /**
     * Method fetching all the phototsets of a certain user - 89405227@N05
     *
     * @return An ArrayList containing those photosets. Null if something went wrong.
     */
    public static ArrayList<FlickrPhotoset> getPhotosets() {

        /*
         * Φτιάχνουμε μία λίστα από ζεύγη κλειδιού-τιμής που θα χρειάζεται το Flickr για να μας επιστρέψει τα δεδομένα.
         */
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair(KEY_FORMAT, "json"));
        data.add(new BasicNameValuePair(KEY_METHOD, "flickr.photosets.getList"));
        data.add(new BasicNameValuePair(KEY_NO_JSON_CALLBACK, "1"));
        data.add(new BasicNameValuePair(KEY_API_KEY,
                "4f1629def199298b6d7b16b686390e01"));
        data.add(new BasicNameValuePair(KEY_USER_ID, "89405227@N05"));
        /*
         * Με τη βοήθεια της executePostRequest επικοινωνούμε με το δίκτυο και παρνουμε την απάντηση με τη μορφή JSON.
         */
        JSONObject responseJson = executePostRequest(BASE_FLICKR_URL, data);

        /*
         * Ξεκινάμε το parsing του JSON που μας επιστράφη, και αν βρούμε κάποια ασυμβατότητα επιστρέφουμε null.
         */
        if (responseJson == null)
            return null;
        JSONObject photosetsJson = responseJson.optJSONObject("photosets");
        if (photosetsJson == null)
            return null;
        JSONArray photosetJsonArray = photosetsJson.optJSONArray("photoset");
        if (photosetJsonArray == null)
            return null;

        ArrayList<FlickrPhotoset> photosets = new ArrayList<FlickrPhotoset>();
        for (int i = 0, lim = photosetJsonArray.length(); i < lim; ++i) {
            /*
             * Διατρέχουμε τον πίνακα από JSONObjects που περιέχουν την πληροφορία για κάθε Photoset,
             * παίρνουμε την πληροφορία αυτή και προσθέτουμε στη λίστα που θα επιστρέψουμε ένα αντικείμενο
             * FlickrPhotoset
             */
            JSONObject photosetJson = photosetJsonArray.optJSONObject(i);
            String title = photosetJson.optJSONObject("title").optString("_content");
            String description = photosetJson.optJSONObject("description").optString(
                    "_content");
            long id = Long.parseLong(photosetJson.optString("id"));
            photosets.add(new FlickrPhotoset(id, title, description));
        }
        return photosets;
    }

    /**
     * Method fetching all the photos of the given photoset
     *
     * @param photosetId The photoset ID, which photos are going to be fetched.
     * @return An ArrayList containing those photos. Null if something went wrong.
     */
    public static ArrayList<FlickrPhoto> getPhotosOfPhotoset(long photosetId) {

        /*
         * Φτιάχνουμε μία λίστα από ζεύγη κλειδιού-τιμής που θα χρειάζεται το Flickr για να μας επιστρέψει τα δεδομένα.
         */
        ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair(KEY_FORMAT, "json"));
        data.add(new BasicNameValuePair(KEY_METHOD,
                "flickr.photosets.getPhotos"));
        data.add(new BasicNameValuePair(KEY_NO_JSON_CALLBACK, "1"));
        data.add(new BasicNameValuePair(KEY_API_KEY,
                "4f1629def199298b6d7b16b686390e01"));
        data.add(new BasicNameValuePair(KEY_USER_ID, "89405227@N05"));
        data.add(new BasicNameValuePair(KEY_PHOTOSET_ID, String
                .valueOf(photosetId)));
        /*
         * Με τη βοήθεια της executePostRequest επικοινωνούμε με το δίκτυο και παρνουμε την απάντηση με τη μορφή JSON.
         */
        final JSONObject responseJson = executePostRequest(BASE_FLICKR_URL, data);

        /*
         * Ξεκινάμε το parsing του JSON που μας επιστράφη, και αν βρούμε κάποια ασυμβατότητα επιστρέφουμε null.
         */
        if (responseJson == null)
            return null;
        JSONObject photosetJson = responseJson.optJSONObject("photoset");
        if (photosetJson == null)
            return null;
        JSONArray photosJsonArray = photosetJson.optJSONArray("photo");
        if (photosJsonArray == null)
            return null;

        ArrayList<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();
        for (int i = 0, lim = photosJsonArray.length(); i < lim; ++i) {
            /*
             * Διατρέχουμε τον πίνακα από JSONObjects που περιέχουν την πληροφορία για κάθε Photo,
             * παίρνουμε την πληροφορία αυτή και προσθέτουμε στη λίστα που θα επιστρέψουμε ένα αντικείμενο
             * FlickrPhoto
             */
            JSONObject photoJson = photosJsonArray.optJSONObject(i);
            String id = photoJson.optString("id");
            String secret = photoJson.optString("secret");
            String server = photoJson.optString("server");
            int farm = photoJson.optInt("farm");
            String title = photoJson.optString("title");
            photos.add(new FlickrPhoto(id, secret, server, farm, title));
        }
        return photos;
    }

    /**
     * Method used to connect to a given URL and execute a POST request with the given key-value pairs as data.
     *
     * @param url  The URL to target the POST request.
     * @param data The key-value pairs to use in the request.
     * @return A JSONObject representing the result from the server. Null if something went wrong.
     */
    private static JSONObject executePostRequest(String url, ArrayList<NameValuePair> data) {

        HttpResponse response;
        try {
            /*
             * Δημιυργούμε έναν HTTP client και εκτελούμε ένα POST request που περιέχει τα δεδομένα που δόθηκαν σε
             * μορφή URL Encoded Form.
             */
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setEntity(new UrlEncodedFormEntity(data));
            response = client.execute(post);
        } catch (IOException e) {
            Log.d(NetworkHelper.class.getSimpleName(), "IO Exception thrown when executing post, " +
                    "possibly due to Internet connection", e);
            return null;
        }

        /*
         * Ελέγχουμε ότι η κλήση στον server ήταν επιτυχημένη, δηλαδή είχε result code 200.
         */
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
            return null;

        try {
            /*
             * Ξεκινάμε να διαβάζουμε την απάντηση από τον server. Χρησιμοποιούμε ένα "διαβαστήρι" που μετατρέπει μία
              * ροή δεδομένων σε χαρακτήρες και διαβάζουμε γραμμή γραμμή. Κάθε γραμμή την προσθέτουμε σε έναν String
              * Buffer.
             */
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer(reader.readLine());
            String str = reader.readLine();
            while (str != null) {
                sb.append("\n").append(str);
                str = reader.readLine();
            }

            /*
             * Τραβάμε όλες τις γραμμές από τον buffer μας και τις μετατρέπουμε σε ένα αντικείμενο JSONObject από
             * απλό κείμενο.
             */
            JSONObject responseJson = new JSONObject(sb.toString());
            return responseJson;
        } catch (IOException e) {
            Log.d(NetworkHelper.class.getSimpleName(), "IO Exception thrown when parsing response", e);
            return null;
        } catch (JSONException e) {
            Log.d(NetworkHelper.class.getSimpleName(), "JSON exception thrown when parsing response into JSON", e);
            return null;
        }
    }
}
