package com.akalipetis.android_workshop_two;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Antonis Kalipetis on 07.12.2013.
 */
public class PhotosGridActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_grid);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        long id = getIntent().getLongExtra("photoset_id", 0);
        PhotosGridFragment f = (PhotosGridFragment) getFragmentManager().findFragmentById(R.id.activity_photos_grid_fragment_photos_grid);
        f.setPhotosetId(id);
    }
}
