package com.akalipetis.android_workshop_two;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FlickrPhotosetAdapter extends BaseAdapter {

    private ArrayList<FlickrPhotoset> mPhotosets;
    private final Context mContext;

    public FlickrPhotosetAdapter(Context context) {
        mContext = context;
    }

    /**
     * Method used to dynamically update the adapter. If null is given, the data of the list gets invalidated,
     * otherwise the list gets notified of the update.
     *
     * @param photosets An ArrayList containing the photosets to use.
     */
    public void setPhotosets(ArrayList<FlickrPhotoset> photosets) {
        mPhotosets = photosets;
        if (mPhotosets == null)
            notifyDataSetInvalidated();
        else
            notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (mPhotosets != null) ? mPhotosets.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mPhotosets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mPhotosets.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_photoset, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        title.setText(mPhotosets.get(position).getTitle());
        description.setText(mPhotosets.get(position).getDescription());
        return convertView;
    }
}
