package com.akalipetis.android_workshop_two;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotosGridFragment extends Fragment {

    /**
     * Method setting the photoset ID to use when fetching photos and populating this Fragment's grid.
     * When this method is called, a task is immediately started to fetch the photots.
     *
     * @param id The photoset ID to use
     */
    public void setPhotosetId(long id) {
        FlickrPhotosetPhotosTask task = new FlickrPhotosetPhotosTask();
        task.execute(id);
    }

    private FlickrPhotosAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*
         * Δημιουργούμε το layout μας, χρησιμοποιώντας τον LayoutInflater που μας δίνεται και το resource έχουμε
         * φτιάξει.
         */
        return inflater.inflate(R.layout.fragment_photos_grid, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        /*
         * Η μέθοδος αυτή καλείται όταν έχει δημιουργηθεί το view μας, το οποίο μας το δίνει και σαν παράμετρο.
         * Βρίσκουμε το grid μέσα στο view και μετά του δίνουμε τον adapter.
         */
        GridView grid = (GridView) view.findViewById(R.id.grid1);
        mAdapter = new FlickrPhotosAdapter(getActivity());
        grid.setAdapter(mAdapter);
    }

    /**
     * Task used to fetch the photos of a given photoset, passed as an ID in execution parameters.
     */
    private class FlickrPhotosetPhotosTask extends
            AsyncTask<Long, Void, ArrayList<FlickrPhoto>> {

        @Override
        protected ArrayList<FlickrPhoto> doInBackground(Long... params) {
            /*
             * Χρησιμοποιούμε το NetworkHelper για να πάρουμε τις φωτογραφίες του δοθέντος photoset - με χρήση του ID
             * που δίνεται ως η πρώτη παράμετρος (params[0]).
             */
            return NetworkHelper.getPhotosOfPhotoset(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<FlickrPhoto> result) {
            super.onPostExecute(result);
            /*
             * Δρούμε ανάλογα με το τι μας επέστρεψε η getPhotosOfPhotoset - το οποίο δίνεται σαν παράμετρος result
             * σε αυτή τη συνάρτηση. Αν δεν είναι null περνάμε τα δεδομένα στον adapter,
             * αλλιώς - δηλαδή αν κάτι πήγε λάθος - τυπώνουμε αντίστοιχο μήνυμα.
             */
            if (result != null) {
                mAdapter.setPhotos(result);
            } else {
                Toast.makeText(getActivity(), "Something went wrong getting your photos...",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
